import SuwaboNew

class Script:
    def __init__(self,name):
        self.name = name

print("""
*********************
* WELCOME TO SUWABO *
*********************
      """)

while True:
    user_input = input("(N)New Script, (L)Load Script: ")

    if user_input.lower() == 'n':
        script_name = input("Enter your script name: ")

        active_script = Script(script_name)
        SuwaboNew.new_script(active_script)

        print("Module for script execution or back to previous command")

        break
        
    elif user_input.lower() == 'l':
        print("Add load module here")

    else:
        break
